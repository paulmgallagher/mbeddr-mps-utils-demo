# Mbeddr MPS Utils Demo

## Setup dependencies

```
mkdir deps
cd deps
curl -o platform-2021.1.23260.4244c2e.zip https://projects.itemis.de/nexus/content/repositories/mbeddr/com/mbeddr/platform/2021.1.23260.4244c2e/platform-2021.1.23260.4244c2e.zip
unzip platform-2021.1.23260.4244c2e.zip
```
